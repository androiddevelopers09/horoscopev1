package com.slantedlines.android.horoscopeapp;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.GestureDetector;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TodayActivity extends AppCompatActivity{

    private View parentView;

    private Post post;
    private Posst posst;
    private MyPostAdapter adapter;

    public  String TAG;
    public String mHoroscope;
    public int mHoroscopeImage;
    public String mHoroscopeDate;

    //UI variables
    GestureDetector gestureDetector;
    public TextView tv_horoscope;
    public TextView tv_horoscopeDate;
    public ImageView image_horoscope;
    public TextView tv_horoscopeContent;
    public TextView tv_facts;
    public TextView tv_facts_title;
    public ProgressBar mprogressBar;
    // Session Manager Class
    SessionManager session;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_today);

        mprogressBar= (ProgressBar) findViewById(R.id.progressbar);

        SimpleDateFormat mdformat=new SimpleDateFormat("yyyy/MM/dd");
        Calendar calendar=Calendar.getInstance();
        String date1=mdformat.format(calendar.getTime());

//        mHoroscope = getIntent().getStringExtra("Horoscope");
//        mHoroscopeImage = getIntent().getIntExtra("Image", 0);
//        mHoroscopeDate = getIntent().getStringExtra("Date");

//        final Bundle bundle=new Bundle();
//        bundle.putString("Horoscope",mHoroscope);
//        bundle.putInt("Image",mHoroscopeImage);
//        bundle.putString("Date",mHoroscopeDate);


        BottomNavigationView bottomNavigationView = (BottomNavigationView)
                findViewById(R.id.bottom_navigation);
        Menu menu = bottomNavigationView.getMenu();
        MenuItem menuItem = menu.getItem(0);
        menuItem.setChecked(true);

        bottomNavigationView.setOnNavigationItemSelectedListener(
                new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        switch (item.getItemId()) {

                            case R.id.action_all_horoscope: {
                                Intent intent2 = new Intent(TodayActivity.this, ViewallActivity.class);
       //                         intent2.putExtras(bundle);
                                startActivity(intent2);
                                finish();
                                break;
                            }
                            case R.id.action_profile: {
                                Intent intent3=new Intent(TodayActivity.this,SettingsActivity.class);
                                startActivity(intent3);
                                finish();
                                break;
                            }

                        }
                        return true;
                    }

                });


//Test session manager
        // Session Manager
        session = new SessionManager(getApplicationContext());
        if (session.isLoggedIn()) {
            // get user data from session
            HashMap<String, String> userpref = session.getUserDetails();

            // horoscope
             mHoroscope = userpref.get(SessionManager.KEY_HOROSCOPE);
             mHoroscopeDate = userpref.get(SessionManager.KEY_DATE);
        }



            post = new Post();
        ////////
        posst = new Posst();

        /**
         * Checking Internet Connection
         */

        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (cm.getActiveNetworkInfo() != null) {
            //if (InternetConnection.checkConnection(getApplicationContext())) {
           // Toast.makeText(this, "Yess Internet", Toast.LENGTH_SHORT).show();
            //Creating an object of our api interface
            ApiService api = RetroClient.getApiService();

            /**
             * Calling JSON
             */
            String day1=date1.substring(8);
            Call<Posst> call = api.getMyJSON2(mHoroscope+ "-"+ day1);
            /**
             * Enqueue Callback will be call when get response...
             */

            call.enqueue(new Callback<Posst>() {
                @Override
                public void onResponse(Call<Posst> call, Response<Posst> response) {

                    if (response.isSuccessful()) {
                        /**
                         * Got Successfully
                         */
                        post = response.body().getPost();
                        generateUIcontent(post);
                        //dialog.dismiss();
                        Log.v("Posst", post.getTitle());

                    } else {
                            Snackbar.make((findViewById(android.R.id.content)),R.string.string_some_thing_wrong + "", Snackbar.LENGTH_SHORT);
                           // Snackbar.make(parentView, R.string.string_some_thing_wrong, Snackbar.LENGTH_LONG).show();

                    }
                }

                @Override
                public void onFailure(Call<Posst> call, Throwable t) {
                    //dialog.dismiss();
                }
            });

        } else {
            mprogressBar.setVisibility(View.INVISIBLE);
            Toast.makeText(this, "No Internet", Toast.LENGTH_LONG).show();
//            Snackbar.make(parentView, R.string.string_internet_connection_not_available, Snackbar.LENGTH_LONG).show();
        }
    }



    private void degenerateUI() {
        tv_horoscope = (TextView) findViewById(R.id.horoscopeName);
        tv_horoscopeDate = (TextView) findViewById(R.id.horoscopeDate);
        image_horoscope = (ImageView) findViewById(R.id.horoscopeImage);
        tv_horoscopeContent = (TextView) findViewById(R.id.horoscopeContent);
        tv_facts=(TextView) findViewById(R.id.horoscopefacts);
        tv_facts_title= (TextView) findViewById(R.id.horoscopefactstitle);

        mprogressBar.setVisibility(View.VISIBLE);
        tv_horoscope.setVisibility(View.INVISIBLE);
        tv_horoscopeDate.setVisibility(View.INVISIBLE);
        image_horoscope.setVisibility(View.INVISIBLE);
        tv_horoscopeContent.setVisibility(View.INVISIBLE);
        tv_facts_title.setVisibility(View.INVISIBLE);
        tv_facts.setVisibility(View.INVISIBLE);

    }

    private void generateUIcontent(Post p) {
//        tv_horoscope = (TextView) findViewById(R.id.horoscopeName);
//        tv_horoscopeDate = (TextView) findViewById(R.id.horoscopeDate);
//        image_horoscope = (ImageView) findViewById(R.id.horoscopeImage);
//        tv_horoscopeContent = (TextView) findViewById(R.id.horoscopeContent);
//        tv_facts=(TextView) findViewById(R.id.horoscopefacts);
//        tv_facts_title= (TextView) findViewById(R.id.horoscopefactstitle);

        Log.v("Sag","###########");
        Log.v("Sag",p.getTitle());
        Log.v("Sag",mHoroscopeDate);
        Log.v("Sag",p.getContent());
        Log.v("Sag",p.getExcerpt());
        Log.v("Sag","###########");

        tv_horoscope.setText(p.getTitle());
        tv_horoscopeDate.setText(mHoroscopeDate);
        Picasso
                .with(this)
                .load(p.getThumbnailImages().getFull().getUrl())
                .fit()
                .into(image_horoscope);
        //image_horoscope.setImageResource(mHoroscopeImage);
        tv_horoscopeContent.setText(p.getContent());
        tv_facts.setText(p.getExcerpt());

        Log.v("GUI","UI generated");

        mprogressBar.setVisibility(View.INVISIBLE);
        tv_horoscope.setVisibility(View.VISIBLE);
        tv_horoscopeDate.setVisibility(View.VISIBLE);
        image_horoscope.setVisibility(View.VISIBLE);
        tv_horoscopeContent.setVisibility(View.VISIBLE);
        tv_facts_title.setVisibility(View.VISIBLE);
        tv_facts.setVisibility(View.VISIBLE);
    }

//////////////////////////////
/*
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.app_menu, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_changeUserInfo: {
                session.logoutUser();
                //session.checkLogin();
                Toast.makeText(this, "Today settings clicked", Toast.LENGTH_SHORT).show();
                finish();

                return true;
            }
            default:
                return super.onOptionsItemSelected(item);
        }
    }

*/
///////////////////////////////

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.v("State","Today onDestroy");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.v("State","Today onPause");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.v("State","Today onStop");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.v("State","Today onStart");
    }

    @Override
    protected void onResume() {
        super.onResume();
        //Make all background invisible
        degenerateUI();
        Log.v("State","Today onResume");

    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        Log.v("State","Today onPostResume");
    }

    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setTitle("Exit?")
                .setMessage("Are you sure you want to exit?")
                .setNegativeButton(R.string.no, null)
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        Log.v("State","Exit pressed from Today");
                        finish();
                    }
                }).create().show();

    }
}
