package com.slantedlines.android.horoscopeapp;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

//import com.squareup.picasso.Picasso;

//import com.bumptech.glide.Glide;

import java.util.List;

/**
 * Created by Aashis on 2/9/2017.
 */

public class MyPostAdapter extends ArrayAdapter<Post> {
    List<Post> postList;
    Context context;
    private LayoutInflater mInflater;
    public MyPostAdapter(Context context, List<Post> objects) {
        super(context,0, objects);
        this.context = context;
        this.mInflater = LayoutInflater.from(context);
        postList = objects;
    }

    @Nullable
    @Override
    public Post getItem(int position) {
        return postList.get(position);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder vh;
        if (convertView == null) {
            View view = mInflater.inflate(R.layout.list_row, parent, false);
            vh = ViewHolder.create((RelativeLayout) view);

            view.setTag(vh);
        } else {
            vh = (ViewHolder) convertView.getTag();
        }

        final Post item = getItem(position);
        vh.imageViewSign.setImageResource(R.drawable.loe_48);
        vh.textViewTitle.setText(item.getTitle());
        vh.textViewDetails.setText(item.getContent());
        vh.textViewFacts.setText(item.getExcerpt());
        Log.v("Text22",item.getExcerpt());
        return vh.rootView;
    }


    private static class ViewHolder {
        public final RelativeLayout rootView;
        public final ImageView imageViewSign;
        public final TextView textViewTitle;
        public final TextView textViewDetails;
        public final TextView textViewFacts;

        public ViewHolder(RelativeLayout rootView, ImageView imageViewSign, TextView textViewTitle, TextView textViewDetails, TextView textViewFacts) {
            this.rootView = rootView;
            this.imageViewSign = imageViewSign;
            this.textViewTitle = textViewTitle;
            this.textViewDetails = textViewDetails;
            this.textViewFacts = textViewFacts;

        }

        public static ViewHolder create(RelativeLayout rootView) {

            ImageView imageView = (ImageView) rootView.findViewById(R.id.horoscopeSign);
            TextView textViewTitle= (TextView) rootView.findViewById(R.id.horoscopeTitle);
            TextView textViewDetails = (TextView) rootView.findViewById(R.id.horoscopeDetails);
            TextView textViewFacts = (TextView) rootView.findViewById(R.id.horoscopefacts);
            return new ViewHolder(rootView, imageView, textViewTitle,textViewDetails,textViewFacts);
        }
    }
}
