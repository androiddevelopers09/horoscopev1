package com.slantedlines.android.horoscopeapp;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Aashis on 2/6/2017.
 */

public interface ApiService {

    /*
 Retrofit get annotation with our URL
 And our method that will return us the List of ContactList
 */
    @GET("get_date_posts/?count=13&")
    Call<Posts> getMyJSON(@Query("date") String date1);

    @GET("get_post/")
    Call<Posst> getMyJSON2(@Query("slug") String slug);  //@Path("id") int id, @Query("api_key") String apiKey
}
