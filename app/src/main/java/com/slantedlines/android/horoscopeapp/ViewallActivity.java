package com.slantedlines.android.horoscopeapp;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.GestureDetector;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ViewallActivity extends AppCompatActivity{

    RecyclerView recyclerView;
    AllPostsAdapter allPostAdapter;
    ArrayList<Post> postList;
    GestureDetector gestureDetector;
    GestureDetectorCompat detectorCompat;
    SessionManager session;


    public String mHoroscope;
    public int mHoroscopeImage;
    public String mHoroscopeDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_viewall);

//        SharedPreferences settings=getSharedPreferences("TEST_PREF",MODE_PRIVATE);
//        // Reading from SharedPreferences
//        String value = settings.getString("key2", "");
//        Toast.makeText(this, value, Toast.LENGTH_SHORT).show();
        // Session class instance
        session = new SessionManager(getApplicationContext());


        BottomNavigationView bottomNavigationView = (BottomNavigationView)
                findViewById(R.id.bottom_navigation);
        Menu menu = bottomNavigationView.getMenu();
        MenuItem menuItem = menu.getItem(1);
        menuItem.setChecked(true);

//        mHoroscope = getIntent().getExtras().getString("Horoscope");
//        mHoroscopeImage = getIntent().getExtras().getInt("Image",0);
//        mHoroscope = getIntent().getExtras().getString("Date");


        bottomNavigationView.setOnNavigationItemSelectedListener(
                new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.action_myhoroscope: {
                                Intent intent1 = new Intent(ViewallActivity.this, TodayActivity.class);
//                                intent1.putExtra("Horoscope", mHoroscope);
//                                intent1.putExtra("Image", mHoroscopeImage);
//                                intent1.putExtra("Date", mHoroscopeDate);
                                startActivity(intent1);
                                finish();
                                break;
                            }

                            case R.id.action_profile: {
                                Intent intent3=new Intent(ViewallActivity.this,SettingsActivity.class);
                                startActivity(intent3);
                                finish();
                                break;
                            }

                        }
                        return true;
                    }

                });

        postList = new ArrayList<>();


        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        SimpleDateFormat mdformat = new SimpleDateFormat("yyyy/MM/dd");
        Calendar calendar = Calendar.getInstance();
        String date1 = mdformat.format(calendar.getTime());

        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (cm.getActiveNetworkInfo() != null) {
            ApiService apiService = RetroClient.getApiService();
            Call<Posts> call = apiService.getMyJSON(date1);
            call.enqueue(new Callback<Posts>() {
                @Override
                public void onResponse(Call<Posts> call, Response<Posts> response) {

                    postList = response.body().getPosts();
                    allPostAdapter = new AllPostsAdapter(getApplicationContext(), postList);
                    recyclerView.setAdapter(allPostAdapter);
                }

                @Override
                public void onFailure(Call<Posts> call, Throwable t) {

                }
            });
        } else {
            Toast.makeText(this, "No Internet", Toast.LENGTH_LONG).show();
        }
    }

/////////////////////////////
/*
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.app_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.action_changeUserInfo: {
//                  Call this function whenever you want to check user login
//                  This will redirect user to LoginActivity is he is not
//                  logged in

                session.logoutUser();
                //session.checkLogin();
                return true;
            }
            default:
                return super.onOptionsItemSelected(item);
        }
    }

*/
//////////////////////////////////////////

//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        switch (item.getItemId()){
//            case android.R.id.home:
//                NavUtils.navigateUpFromSameTask(this);
//                return true;
//        }
//        return super.onOptionsItemSelected(item);
//
//    }



    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.v("State","Viewall onDestroy");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.v("State","Viewall onPause");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.v("State","Viewall onStop");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.v("State","Viewall onStart");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.v("State","Viewall onResume");

    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        Log.v("State","Viewall onPostResume");
    }

    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setTitle("Exit?")
                .setMessage("Are you sure you want to exit?")
                .setNegativeButton(R.string.no, null)
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        Log.v("State","Exit pressed from Viewall");
                       finish();
                    }
                }).create().show();

    }

}

