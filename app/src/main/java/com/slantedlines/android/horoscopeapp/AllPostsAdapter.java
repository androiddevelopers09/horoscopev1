package com.slantedlines.android.horoscopeapp;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

public class AllPostsAdapter extends RecyclerView.Adapter<AllPostsAdapter.AllPostViewHolder>{
    List<Post> postList;
    Context context;
    LayoutInflater mInflater;
    Post postItem;
    public AllPostsAdapter(Context context, List<Post> objects) {
        //super(context,objects);
        this.context = context;
        this.mInflater = LayoutInflater.from(context);
        postList = objects;
    }

    @Override
    public AllPostViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view=mInflater.inflate(R.layout.list_row,parent,false);
        return new AllPostViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AllPostViewHolder holder, int position) {

        postItem=postList.get(position);

        if (postItem.getThumbnailImages()==null)
        {   Picasso
                .with(context)
                .load(R.drawable.loe_48)
                .fit()
                .into(holder.imageViewHoroscope);}
        else {
            Picasso
                    .with(context)
                    .load(postItem.getThumbnailImages().getFull().getUrl())
                    .fit()
                    .into(holder.imageViewHoroscope);}
            holder.textViewTitle.setText(postItem.getTitle());
            holder.textViewDetails.setText(postItem.getContent());
    }

    @Override
    public int getItemCount() {
        return postList.size();
    }

    public class AllPostViewHolder extends RecyclerView.ViewHolder
    {
        ImageView imageViewHoroscope;
        TextView textViewTitle,textViewDetails;

        public AllPostViewHolder(View itemView) {
            super(itemView);
            imageViewHoroscope= (ImageView) itemView.findViewById(R.id.horoscopeSign);
            textViewTitle= (TextView) itemView.findViewById(R.id.horoscopeTitle);
            textViewDetails= (TextView) itemView.findViewById(R.id.horoscopeDetails);
        }
    }
}
