package com.slantedlines.android.horoscopeapp;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

//@SerializedNameOrder({
//"id",
//"type",
//"slug",
//"url",
//"status",
//"title",
//"title_plain",
//"content",
//"excerpt",
//"date",
//"modified",
//"categories",
//"tags",
//"author",
//"comments",
//"attachments",
//"comment_count",
//"comment_status",
//"thumbnail",
//"custom_fields",
//"thumbnail_size",
//"thumbnail_images"
//})
public class Post {


@SerializedName("id")
@Expose
private Integer id;

@SerializedName("type")
@Expose
private String type;

@SerializedName("slug")
@Expose
private String slug;

@SerializedName("url")
@Expose
private String url;

@SerializedName("status")
@Expose
private String status;

@SerializedName("title")
@Expose
private String title;

@SerializedName("title_plain")
@Expose
private String titlePlain;

@SerializedName("content")
@Expose
private String content;

@SerializedName("excerpt")
@Expose
private String excerpt;

@SerializedName("date")
@Expose
private String date;

@SerializedName("modified")
@Expose
private String modified;

@SerializedName("categories")
@Expose
private List<Object> categories = null;

@SerializedName("tags")
@Expose
private List<Object> tags = null;

//@SerializedName("author")
//@Expose
//private Author author;

//@SerializedName("comments")
//@Expose
//private List<com.slantedlines.android.retrofithr.Comment> comments = null;

@SerializedName("attachments")
@Expose
private List<Object> attachments = null;

@SerializedName("comment_count")
@Expose
private Integer commentCount;

@SerializedName("comment_status")
@Expose
private String commentStatus;

//@SerializedName("thumbnail")
//@Expose
//private Thumbnail thumbnail;

//@SerializedName("custom_fields")
//@Expose
//private CustomFields customFields;
//
//@SerializedName("thumbnail_size")
//@Expose
//private String thumbnailSize;

@SerializedName("thumbnail_images")
@Expose
private ThumbnailImages thumbnailImages;




@SerializedName("id")
public Integer getId() {
return id;
}

@SerializedName("id")
public void setId(Integer id) {
this.id = id;
}

@SerializedName("type")
public String getType() {
return type;
}

@SerializedName("type")
public void setType(String type) {
this.type = type;
}

@SerializedName("slug")
public String getSlug() {
return slug;
}

@SerializedName("slug")
public void setSlug(String slug) {
this.slug = slug;
}

@SerializedName("url")
public String getUrl() {
return url;
}

@SerializedName("url")
public void setUrl(String url) {
this.url = url;
}

@SerializedName("status")
public String getStatus() {
return status;
}

@SerializedName("status")
public void setStatus(String status) {
this.status = status;
}

@SerializedName("title")
public String getTitle() {
return title;
}

@SerializedName("title")
public void setTitle(String title) {
this.title = title;
}

@SerializedName("title_plain")
public String getTitlePlain() {
return titlePlain;
}

@SerializedName("title_plain")
public void setTitlePlain(String titlePlain) {
this.titlePlain = titlePlain;
}

@SerializedName("content")
public String getContent() {
return content;
}

@SerializedName("content")
public void setContent(String content) {
this.content = content;
}

@SerializedName("excerpt")
public String getExcerpt() {
return excerpt;
}

@SerializedName("excerpt")
public void setExcerpt(String excerpt) {
this.excerpt = excerpt;
}

@SerializedName("date")
public String getDate() {
return date;
}

@SerializedName("date")
public void setDate(String date) {
this.date = date;
}

@SerializedName("modified")
public String getModified() {
return modified;
}

@SerializedName("modified")
public void setModified(String modified) {
this.modified = modified;
}

@SerializedName("categories")
public List<Object> getCategories() {
return categories;
}

@SerializedName("categories")
public void setCategories(List<Object> categories) {
this.categories = categories;
}

@SerializedName("tags")
public List<Object> getTags() {
return tags;
}

@SerializedName("tags")
public void setTags(List<Object> tags) {
this.tags = tags;
}


//@SerializedName("author")
//public Author getAuthor() {
//return author;
//}
//
//@SerializedName("author")
//public void setAuthor(Author author) {
//this.author = author;
//}

//@SerializedName("comments")
//public List<Comment> getComments() {
//return comments;
//}
//
//@SerializedName("comments")
//public void setComments(List<Comment> comments) {
//this.comments = comments;
//}

@SerializedName("attachments")
public List<Object> getAttachments() {
return attachments;
}

@SerializedName("attachments")
public void setAttachments(List<Object> attachments) {
this.attachments = attachments;
}

@SerializedName("comment_count")
public Integer getCommentCount() {
return commentCount;
}

@SerializedName("comment_count")
public void setCommentCount(Integer commentCount) {
this.commentCount = commentCount;
}

@SerializedName("comment_status")
public String getCommentStatus() {
return commentStatus;
}

@SerializedName("comment_status")
public void setCommentStatus(String commentStatus) {
this.commentStatus = commentStatus;
}

//@SerializedName("thumbnail")
//public Thumbnail getThumbnail() {
//return thumbnail;
//}
//
//@SerializedName("thumbnail")
//public void setThumbnail(Thumbnail thumbnail) {
//this.thumbnail = thumbnail;
//}
//
//@SerializedName("custom_fields")
//public CustomFields getCustomFields() {
//return customFields;
//}
//
//@SerializedName("custom_fields")
//public void setCustomFields(CustomFields customFields) {
//this.customFields = customFields;
//}
//
//@SerializedName("thumbnail_size")
//public String getThumbnailSize() {
//return thumbnailSize;
//}
//
//@SerializedName("thumbnail_size")
//public void setThumbnailSize(String thumbnailSize) {
//this.thumbnailSize = thumbnailSize;
//}

@SerializedName("thumbnail_images")
public ThumbnailImages getThumbnailImages() {
return thumbnailImages;
}

@SerializedName("thumbnail_images")
public void setThumbnailImages(ThumbnailImages thumbnailImages) {
this.thumbnailImages = thumbnailImages;
}
    public class ThumbnailImages {


        @SerializedName("full")
        private Full full;

        @SerializedName("full")
        public Full getFull() {
            return full;
        }

        @SerializedName("full")
        public void setFull(Full full) {
            this.full = full;
        }

    }


    public class Full {

        @SerializedName("url")
        @Expose
        private String url;

        @SerializedName("width")
        @Expose
        private Integer width;

        @SerializedName("height")
        @Expose
        private Integer height;

        @SerializedName("url")
        public String getUrl() {
            return url;
        }

        @SerializedName("url")
        public void setUrl(String url) {
            this.url = url;
        }

        @SerializedName("width")
        public Integer getWidth() {
            return width;
        }

        @SerializedName("width")
        public void setWidth(Integer width) {
            this.width = width;
        }

        @SerializedName("height")
        public Integer getHeight() {
            return height;
        }

        @SerializedName("height")
        public void setHeight(Integer height) {
            this.height = height;
        }
    }

}
