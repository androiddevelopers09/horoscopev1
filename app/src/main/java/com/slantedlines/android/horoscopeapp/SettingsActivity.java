package com.slantedlines.android.horoscopeapp;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Calendar;
import java.util.HashMap;


public class SettingsActivity extends AppCompatActivity {

    SessionManager session;
    String user, dob;
    DatePicker datePicker;
    TextView changeUserInfo;
    TextView username, dateofbirth;
    Dialog customDialogue;
    Button btnSubmit;
    static final int DATE_DIALOG_ID = 999;

    public int year, month, day;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);


        datePicker = (DatePicker) findViewById(R.id.dpresult);
        btnSubmit = (Button) findViewById(R.id.submit);

        btnSubmit.setTextColor(getResources().getColor(R.color.datewhite));

        //TextView for Data
        username = (TextView) findViewById(R.id.txtUser);
        dateofbirth = (TextView) findViewById(R.id.txtdob);

        //Setting Date Picker View in UI
        // setCurrentDateOnView();

        //Get Data from date picker
        //   getDatePickerData();
        // Session Manager
        session = new SessionManager(getApplicationContext());
        if (session.isLoggedIn()) {
            // get user data from session
            HashMap<String, String> userpref = session.getUserDetails();

            // horoscope
            user = userpref.get(SessionManager.KEY_NAME);
            username.setText(user);
            dob = userpref.get(SessionManager.KEY_DOB);
            dateofbirth.setText(dob);
        }

//        changeUserInfo= (TextView) findViewById(R.id.edit_dob);
//        changeUserInfo.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                datePicker.setVisibility(View.VISIBLE);
//                btnSubmit.setVisibility(View.VISIBLE);
//            }
//        });

        //Setting Date Picker View in UI
        setCurrentDateOnView();

        BottomNavigationView bottomNavigationView = (BottomNavigationView)
                findViewById(R.id.bottom_navigation);
        Menu menu = bottomNavigationView.getMenu();
        MenuItem menuItem = menu.getItem(2);
        menuItem.setChecked(true);


        bottomNavigationView.setOnNavigationItemSelectedListener(
                new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.action_myhoroscope: {
                                Intent intent1 = new Intent(SettingsActivity.this, TodayActivity.class);
                                //intent2.putExtras(bundle);
                                startActivity(intent1);
                                finish();
                                break;
                            }

                            case R.id.action_all_horoscope: {
                                Intent intent2 = new Intent(SettingsActivity.this, ViewallActivity.class);
                                //                         intent2.putExtras(bundle);
                                startActivity(intent2);
                                finish();
                                break;
                            }

                        }
                        return true;
                    }

                });
        username.setOnClickListener(new View.OnClickListener() {
            //Custom Dialogue variables
            EditText Lname;
            Button savebtn, canbtn;

            @Override
            public void onClick(View view) {
                //Toast.makeText(SettingsActivity.this, "Usernamem clicked", Toast.LENGTH_SHORT).show();
                customDialogue = new Dialog(SettingsActivity.this);
                customDialogue.requestWindowFeature(Window.FEATURE_NO_TITLE);
                customDialogue.setContentView(R.layout.dialogue);
                view = getWindow().getDecorView();
                view.setBackgroundResource(android.R.color.transparent);

                Lname = (EditText) customDialogue.findViewById(R.id.lname);
                savebtn = (Button) customDialogue.findViewById(R.id.savebtn);
                canbtn = (Button) customDialogue.findViewById(R.id.canbtn);

                savebtn.setOnClickListener(new View.OnClickListener() {
                    String saved_horoscope, saved_horoscopedate, saved_dob;

                    @Override
                    public void onClick(View view) {
                        user = Lname.getText().toString();
                        username.setText(user);
                        saved_horoscope = session.getUserDetails().get(SessionManager.KEY_HOROSCOPE);
                        saved_horoscopedate = session.getUserDetails().get(SessionManager.KEY_DATE);
                        saved_dob = session.getUserDetails().get(SessionManager.KEY_DOB);
                        session.edit();
                        session.createLoginSession(user, saved_horoscope, saved_horoscopedate, saved_dob);
                        customDialogue.dismiss();
                    }
                });
                canbtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        customDialogue.dismiss();
                    }
                });
                customDialogue.show();
            }
        });

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnSubmit.setTextColor(getResources().getColor(R.color.datewhite));
                year = datePicker.getYear();
                Log.v("year", Integer.toString(year));
                month = datePicker.getMonth() + 1;
                day = datePicker.getDayOfMonth();


                String mHoroscope = null;
                String mHoroscopeDate = null;
                int mHoroscopeImage = 0;

/**** Zodiac Sign Finder Java code****/

                if ((month == 12 && day >= 22 && day <= 31) || (month == 1 && day >= 1 && day <= 19)) {
                    mHoroscope = "Capricorn";
                    mHoroscopeDate = "(Dec 22 - Jan 19)";
                } else if ((month == 1 && day >= 20 && day <= 31) || (month == 2 && day >= 1 && day <= 17)) {
                    mHoroscope = "Aquarius";
                    mHoroscopeDate = "(Jan 20 - Feb 17)";
                } else if ((month == 2 && day >= 18 && day <= 29) || (month == 3 && day >= 1 && day <= 19)) {
                    mHoroscope = "Pisces";
                    mHoroscopeDate = "(Feb 18 - Mar 19)";
                } else if ((month == 3 && day >= 20 && day <= 31) || (month == 4 && day >= 1 && day <= 19)) {
                    mHoroscope = "Aries";
                    mHoroscopeDate = "(Mar 20 - Apr 19)";
                } else if ((month == 4 && day >= 20 && day <= 30) || (month == 5 && day >= 1 && day <= 20)) {
                    mHoroscope = "Taurus";
                    mHoroscopeDate = "(Apr 20 - May 20)";
                } else if ((month == 5 && day >= 21 && day <= 31) || (month == 6 && day >= 1 && day <= 20)) {
                    mHoroscope = "Gemini";
                    mHoroscopeDate = "(May 21 - Jun 20)";
                } else if ((month == 6 && day >= 21 && day <= 30) || (month == 7 && day >= 1 && day <= 22)) {
                    mHoroscope = "Cancer";
                    mHoroscopeDate = "(Jun 21 - Jul 22)";
                } else if ((month == 7 && day >= 23 && day <= 31) || (month == 8 && day >= 1 && day <= 22)) {
                    mHoroscope = "Leo";
                    mHoroscopeDate = "(Jul 23 - Aug 22)";
                } else if ((month == 8 && day >= 23 && day <= 31) || (month == 9 && day >= 1 && day <= 22)) {
                    mHoroscope = "Virgo";
                    mHoroscopeDate = "(Aug 23 - Sep 22)";
                } else if ((month == 9 && day >= 23 && day <= 30) || (month == 10 && day >= 1 && day <= 22)) {
                    mHoroscope = "Libra";
                    mHoroscopeDate = "(Sep 23 - Oct 22)";
                } else if ((month == 10 && day >= 23 && day <= 31) || (month == 11 && day >= 1 && day <= 21)) {
                    mHoroscope = "Scorpio";
                    mHoroscopeDate = "(Oct 23 - Nov 21)";
                } else if ((month == 11 && day >= 22 && day <= 30) || (month == 12 && day >= 1 && day <= 21)) {
                    mHoroscope = "Saggitarrius";
                    mHoroscopeDate = "(Nov 22 - Dec 21)";
                } else {
                    finish();

                }

                /**** Zodiac Sign Finder Java code****/
//                Log.v("Month", Integer.toString(month));
                String dob = year + "/" + month + "/" + day;
                String saved_user;
                saved_user = session.getUserDetails().get(SessionManager.KEY_NAME);
                dateofbirth.setText(dob);
                session.edit();
                session.createLoginSession(saved_user, mHoroscope, mHoroscopeDate, dob);

                showLocationDialog(saved_user, mHoroscope, mHoroscopeImage, mHoroscopeDate, dob);
            }
        });
    }

    private void showLocationDialog(final String user_name, final String user_horoscope, final int image, final String date, final String dob) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(SettingsActivity.this);
        builder.setTitle("Hey! " + user_name);
        builder.setMessage("Your are " + user_horoscope);

        String positiveText = getString(android.R.string.ok);
        builder.setPositiveButton(positiveText,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // positive button logic

                        dialog.dismiss();
                        session.createLoginSession(user_name, user_horoscope, date, dob);
                        Intent intent = new Intent(SettingsActivity.this, TodayActivity.class);
                        startActivity(intent);
                        finish();

                    }
                });


        AlertDialog dialog = builder.create();
        // display dialog
        dialog.show();
    }

    public void setCurrentDateOnView() {
        datePicker = (DatePicker) findViewById(R.id.dpresult);
        final Calendar c = Calendar.getInstance();
        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DAY_OF_MONTH);
//        // set current date into textview
//        tv.setText(new StringBuilder()
//                // Month is 0 based, just add 1
//                .append(month + 1).append("-").append(day).append("-")
//                .append(year).append(" "));

        // set current date into datepicker

        Log.v("Month", Integer.toString(month));
        datePicker.init(year, month, day, null);
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case DATE_DIALOG_ID:
                // set date picker as current date
                return new DatePickerDialog(this, datePickerListener,
                        year, month, day);
        }
        return null;
    }

    private DatePickerDialog.OnDateSetListener datePickerListener
            = new DatePickerDialog.OnDateSetListener() {

        // when dialog box is closed, below method will be called.
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {
//
//            loginpage.setBackgroundColor(getResources().getColor(R.color.setwhite));
//            datePicker.setBackgroundColor(getResources().getColor(R.color.datewhite));
            year = selectedYear;
            month = selectedMonth;
            day = selectedDay;
            // set selected date into textview
            // set selected date into datepicker also
            datePicker.init(year, month, day, null);
        }
    };

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.v("State", "Settings onDestroy");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.v("State", "Settings onPause");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.v("State", "Settings onStop");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.v("State", "Settings onStart");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.v("State", "Settings onResume");

    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        Log.v("State", "Settings onPostResume");
    }

    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setTitle("Exit?")
                .setMessage("Are you sure you want to exit?")
                .setNegativeButton(R.string.no, null)
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Log.v("State", "Exit pressed from Settings");
                        finish();
                    }
                }).create().show();

    }
//////////////////////////////
/*
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.app_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_changeUserInfo: {
                session.logoutUser();
                //session.checkLogin();
                finish();
                return true;
            }
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    */
// //////////////////////////////////////////

}
