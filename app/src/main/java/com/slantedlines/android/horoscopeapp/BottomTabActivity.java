package com.slantedlines.android.horoscopeapp;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

public class BottomTabActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bottom_tab);


//       final String mHoroscope = getIntent().getStringExtra("Horoscope");
//        final Integer mHoroscopeImage = getIntent().getIntExtra("Image", 0);
//        final String mHoroscopeDate = getIntent().getStringExtra("Date");


        Intent intent1 = new Intent(BottomTabActivity.this, TodayActivity.class);
//        intent1.putExtra("Horoscope", mHoroscope);
//        intent1.putExtra("Image", mHoroscopeImage);
//        intent1.putExtra("Date", mHoroscopeDate);
        startActivity(intent1);
        this.finish();

        BottomNavigationView bottomNavigationView = (BottomNavigationView)
                findViewById(R.id.bottom_navigation);

        Menu menu=bottomNavigationView.getMenu();
        MenuItem menuItem=menu.getItem(0);
        menuItem.setChecked(true);
        bottomNavigationView.setOnNavigationItemSelectedListener(
                new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.action_myhoroscope: {
                                Intent intent1 = new Intent(BottomTabActivity.this, TodayActivity.class);
//                                intent1.putExtra("Horoscope", mHoroscope);
//                                intent1.putExtra("Image", mHoroscopeImage);
//                                intent1.putExtra("Date", mHoroscopeDate);
                                startActivity(intent1);
                                finish();
                                break;
                            }

                            case R.id.action_all_horoscope: {
                                Intent intent2 = new Intent(BottomTabActivity.this, ViewallActivity.class);
                                startActivity(intent2);
                                finish();
                                break;
                            }
                            case R.id.action_profile: {
                                Intent intent3 = new Intent(BottomTabActivity.this, SettingsActivity.class);
                                startActivity(intent3);
                                finish();
                                break;
                            }

                        }
                        return true;
                    }

                });
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.v("State","Bottom onDestroy");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.v("State","Bottom onPause");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.v("State","Bottom onStop");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.v("State","Bottom onStart");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.v("State","Bottom onResume");

    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        Log.v("State","Bottom onPostResume");
    }

}
