package com.slantedlines.android.horoscopeapp;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Calendar;
import java.util.HashMap;

public class LoginActivity extends AppCompatActivity {

    //UI variables
    private DatePicker datePicker;
    EditText userName;
    Button buttonLogin;
    TextView tv;
    public String TAG = "LoginActivity";

    //Date variable
    private int day, month, year;
    private int m, d;

    static final int DATE_DIALOG_ID = 999;

    // Session Manager Class
    SessionManager session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Session Manager
        session = new SessionManager(getApplicationContext());
        if (session.isLoggedIn()) {
            // get user data from session
            HashMap<String, String> user = session.getUserDetails();

            // name
            String name = user.get(SessionManager.KEY_NAME);
            // horoscope
            String horoscope = user.get(SessionManager.KEY_HOROSCOPE);


            Intent intent = new Intent(LoginActivity.this, BottomTabActivity.class);
//            intent.putExtra("Horoscope", horoscope);
//            intent.putExtra("Image", R.drawable.capricorn);
//            intent.putExtra("Date", "22");
            startActivity(intent);
            finish();

        } else {
            setContentView(R.layout.activity_login);

            //Initializing UI components
            userName = (EditText) findViewById(R.id.et_name);
            buttonLogin = (Button) findViewById(R.id.loginButton);
            buttonLogin.setTextColor(getResources().getColor(R.color.datewhite));
            tv = (TextView) findViewById(R.id.tv);

            //Setting Date Picker View in UI
            setCurrentDateOnView();

            userName.setText(session.getUserDetails().get(SessionManager.KEY_NAME));


            buttonLogin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //Get Login Data
                    // name
//                    buttonLogin.setTextColor(getResources().getColor(R.color.horoscope));
                    String name = userName.getText().toString();
                    year=datePicker.getYear();
                    Log.v("year",Integer.toString(year));
                    month = datePicker.getMonth() + 1;
                    day = datePicker.getDayOfMonth();


                    String mHoroscope = null;
                    String mHoroscopeDate = null;
                    int mHoroscopeImage = 0;

/**** Zodiac Sign Finder Java code****/

                    if ((month == 12 && day >= 22 && day <= 31) || (month == 1 && day >= 1 && day <= 19)) {
                        mHoroscope = "Capricorn";
                        mHoroscopeDate = "(Dec 22 - Jan 19)";
                    } else if ((month == 1 && day >= 20 && day <= 31) || (month == 2 && day >= 1 && day <= 17)) {
                        mHoroscope = "Aquarius";
                        mHoroscopeDate = "(Jan 20 - Feb 17)";
                    } else if ((month == 2 && day >= 18 && day <= 29) || (month == 3 && day >= 1 && day <= 19)) {
                        mHoroscope = "Pisces";
                        mHoroscopeDate = "(Feb 18 - Mar 19)";
                    } else if ((month == 3 && day >= 20 && day <= 31) || (month == 4 && day >= 1 && day <= 19)) {
                        mHoroscope = "Aries";
                        mHoroscopeDate = "(Mar 20 - Apr 19)";
                    } else if ((month == 4 && day >= 20 && day <= 30) || (month == 5 && day >= 1 && day <= 20)) {
                        mHoroscope = "Taurus";
                        mHoroscopeDate = "(Apr 20 - May 20)";
                    } else if ((month == 5 && day >= 21 && day <= 31) || (month == 6 && day >= 1 && day <= 20)) {
                        mHoroscope = "Gemini";
                        mHoroscopeDate = "(May 21 - Jun 20)";
                    } else if ((month == 6 && day >= 21 && day <= 30) || (month == 7 && day >= 1 && day <= 22)) {
                        mHoroscope = "Cancer";
                        mHoroscopeDate = "(Jun 21 - Jul 22)";
                    } else if ((month == 7 && day >= 23 && day <= 31) || (month == 8 && day >= 1 && day <= 22)) {
                        mHoroscope = "Leo";
                        mHoroscopeDate = "(Jul 23 - Aug 22)";
                    } else if ((month == 8 && day >= 23 && day <= 31) || (month == 9 && day >= 1 && day <= 22)) {
                        mHoroscope = "Virgo";
                        mHoroscopeDate = "(Aug 23 - Sep 22)";
                    } else if ((month == 9 && day >= 23 && day <= 30) || (month == 10 && day >= 1 && day <= 22)) {
                        mHoroscope = "Libra";
                        mHoroscopeDate = "(Sep 23 - Oct 22)";
                    } else if ((month == 10 && day >= 23 && day <= 31) || (month == 11 && day >= 1 && day <= 21)) {
                        mHoroscope = "Scorpio";
                        mHoroscopeDate = "(Oct 23 - Nov 21)";
                    } else if ((month == 11 && day >= 22 && day <= 30) || (month == 12 && day >= 1 && day <= 21)) {
                        mHoroscope = "Saggitarrius";
                        mHoroscopeDate = "(Nov 22 - Dec 21)";
                    } else {
                        finish();

                    }

                    /**** Zodiac Sign Finder Java code****/
//                Log.v("Month", Integer.toString(month));

                    String dob=year+"/"+month+"/"+day;
                    showLocationDialog(name, mHoroscope, mHoroscopeImage, mHoroscopeDate,dob);

                }
            });
        }
    }

    private void showLocationDialog(final String user_name, final String user_horoscope, final int image, final String date,final String dob) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
        builder.setTitle("Hey! " + user_name);
        builder.setMessage("Your are " + user_horoscope);

        String positiveText = getString(android.R.string.ok);
        builder.setPositiveButton(positiveText,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // positive button logic

                        dialog.dismiss();
                        if (user_name.trim().length() > 0) {

                            // Creating user login session
                            // For testing i am stroing name, email as follow
                            // Use user real data
                            session.createLoginSession(user_name, user_horoscope,date,dob);
                            Intent intent = new Intent(LoginActivity.this, BottomTabActivity.class);
//                            intent.putExtra("Horoscope", user_horoscope);
//                            intent.putExtra("Image", image);
//                            intent.putExtra("Date", date);
                            startActivity(intent);
                            finish();
                        } else {
                           // Toast.makeText(LoginActivity.this, "Please provide your USERNAME", Toast.LENGTH_LONG).show();
                        }
                    }
                });


        AlertDialog dialog = builder.create();
        // display dialog
        dialog.show();
    }

    public void setCurrentDateOnView() {


        datePicker = (DatePicker) findViewById(R.id.dpresult);

        final Calendar c = Calendar.getInstance();
        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DAY_OF_MONTH);
//        // set current date into textview
//        tv.setText(new StringBuilder()
//                // Month is 0 based, just add 1
//                .append(month + 1).append("-").append(day).append("-")
//                .append(year).append(" "));

        // set current date into datepicker

        Log.v("Month", Integer.toString(month));
        datePicker.init(year, month, day, null);
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case DATE_DIALOG_ID:
                // set date picker as current date
                return new DatePickerDialog(this, datePickerListener,
                        year, month, day);
        }
        return null;
    }

    private DatePickerDialog.OnDateSetListener datePickerListener
            = new DatePickerDialog.OnDateSetListener() {

        // when dialog box is closed, below method will be called.
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {
//
//            loginpage.setBackgroundColor(getResources().getColor(R.color.setwhite));
//            datePicker.setBackgroundColor(getResources().getColor(R.color.datewhite));
            year = selectedYear;
            month = selectedMonth;
            day = selectedDay;
            if (userName.getText().toString().trim().length() < 0) {
                Toast.makeText(LoginActivity.this, "Please provide your USERNAME", Toast.LENGTH_LONG).show();
            }
            // set selected date into textview
            // set selected date into datepicker also
            datePicker.init(year, month, day, null);
        }
    };



    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.v("State","Login onDestroy");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.v("State","Login onPause");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.v("State","Login onStop");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.v("State","Login onStart");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.v("State","Login onResume");

    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        Log.v("State","Login onPostResume");
    }
    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setTitle("Exit?")
                .setMessage("Are you sure you want to exit?")
                .setNegativeButton(R.string.no, null)
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        Log.v("State","Exit pressed from LoginScreen");
                        finish();
                        //System.exit(0);
                    }
                }).create().show();

    }

}
