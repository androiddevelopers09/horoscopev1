package com.slantedlines.android.horoscopeapp;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

//@JsonInclude(JsonInclude.Include.NON_NULL)
//@SerializedNameOrder({
//"status",
//"post",
//"previous_url",
//"next_url"
//})
public class Posst {

    @SerializedName("status")
    @Expose
    private String status;

    @SerializedName("post")
    @Expose
    private Post post;

    @SerializedName("previous_url")
    @Expose
    private String previousUrl;
    @SerializedName("next_url")
    @Expose
    private String nextUrl;

    @SerializedName("status")
    public String getStatus() {
        return status;
    }

    @SerializedName("status")
    public void setStatus(String status) {
        this.status = status;
    }

    @SerializedName("post")
    public Post getPost() {
        return post;
    }

    @SerializedName("post")
    public void setPost(Post post) {
        this.post = post;
    }

    @SerializedName("previous_url")
    public String getPreviousUrl() {
        return previousUrl;
    }

    @SerializedName("previous_url")
    public void setPreviousUrl(String previousUrl) {
        this.previousUrl = previousUrl;
    }

    @SerializedName("next_url")
    public String getNextUrl() {
        return nextUrl;
    }

    @SerializedName("next_url")
    public void setNextUrl(String nextUrl) {
        this.nextUrl = nextUrl;
    }
}