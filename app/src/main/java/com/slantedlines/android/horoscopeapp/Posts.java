package com.slantedlines.android.horoscopeapp;

import java.util.ArrayList;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

//@JsonInclude(JsonInclude.Include.NON_NULL)
//@SerializedNameOrder({
//        "status",
//        "count",
//        "count_total",
//        "pages",
//        "posts",
//        "query"
//})
public class Posts {

    @SerializedName("status")
    @Expose
    private String status;

    @SerializedName("count")
    @Expose
    private Integer count;

    @SerializedName("count_total")
    @Expose
    private Integer countTotal;

    @SerializedName("pages")
    @Expose
    private Integer pages;

//    @SerializedName("posts")
//    @Expose
//    private List<Post> posts = null;

//    @SerializedName("query")
//    @Expose
//    private Query query;
//

    @SerializedName("status")
    public String getStatus() {
        return status;
    }

    @SerializedName("status")
    public void setStatus(String status) {
        this.status = status;
    }

    @SerializedName("count")
    public Integer getCount() {
        return count;
    }

    @SerializedName("count")
    public void setCount(Integer count) {
        this.count = count;
    }

    @SerializedName("count_total")
    public Integer getCountTotal() {
        return countTotal;
    }

    @SerializedName("count_total")
    public void setCountTotal(Integer countTotal) {
        this.countTotal = countTotal;
    }

    @SerializedName("pages")
    public Integer getPages() {
        return pages;
    }

    @SerializedName("pages")
    public void setPages(Integer pages) {
        this.pages = pages;
    }
//
//    @SerializedName("posts")
//    public List<Post> getPosts() {
//        return posts;
//    }
//
//    @SerializedName("posts")
//    public void setPosts(List<Post> posts) {
//        this.posts = posts;
//    }

//    @SerializedName("query")
//    public Query getQuery() {
//        return query;
//    }
//
//    @SerializedName("query")
//    public void setQuery(Query query) {
//        this.query = query;
//    }

    @SerializedName("posts")
    @Expose
    private ArrayList<Post> posts = new ArrayList<>();

    /**
     * @return The contacts
     */
    public ArrayList<Post> getPosts() {
        return posts;
    }


    public void setPosts(ArrayList<Post> posts) {
        this.posts = posts;
    }


}
